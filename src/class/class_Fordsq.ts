export class Foreqdsq{
    dsqobj: number
    fn: () => void
    constructor(fn:()=>void){
        this.dsqobj=0
        this.fn=fn
    }
    init(){
        this.dsqobj=requestAnimationFrame(()=>{
            this.fn()
            this.init()
        })
    }
    stop(){
        cancelAnimationFrame(this.dsqobj)
    }
}
