import axios, { AxiosRequestConfig, AxiosResponse } from "axios"
import qs from "qs"
//字体调节
export function winFont(num: number) {
    const rootFontSize = (document.body.clientWidth / num) * 16;
    document.getElementsByTagName("html")[0].style.fontSize = rootFontSize + "px"
    window.addEventListener("resize", () => {
        const rootFontSize = (document.body.clientWidth / num) * 16
        document.getElementsByTagName("html")[0].style.fontSize = rootFontSize + "px"
    });
}
//获取图片路径
export function getimg(name: string) {
    return new URL(`../assets/img/${name}`, import.meta.url).href;
}
//dom操作
export function dom(res: string) {
    const [str, fir, end] = [res.substring(1), res.charAt(0), res.charAt(res.length - 1)]
    switch (true) {
        case fir == '.': return document.querySelector(str)
        case fir == '&': return document.querySelectorAll(str)
        case fir == '#': return document.getElementById(str)
        case fir == '<' && end == '>': return document.getElementsByTagName(res.substring(1, res.length - 1))
        case fir == '(' && end == ')': return document.createElement(res.substring(1, res.length - 1))
        default:
            throw '操作失败!'
    }
}
//金币格式
export function moneystr(str: string) {
    return str.replace(/(?=\B(\d{3})+$)/g, ',')
}
//ts遍历对象 
export function blobj(obj: object, f: any): void {
    Object.entries(obj).forEach(([_a, _b]) => {
        return f(_a, _b);
    })
}
//函数防抖 // customRef:提供ref get set方法的接口
export function debounceRef(value: any, _time = 1000) {
    let dsq: number;
    return customRef((track, trigger) => {
        return {
            get() {
                track()
                return value;
            },
            set(val: string) {
                clearTimeout(dsq)
                dsq = setTimeout(() => {
                    value = val
                    trigger()
                }, _time)
            }
        }
    })
}
//插入一个dom元素
export function inserdom(pardom: HTMLElement, createdom: HTMLElement, index = 0) {
    pardom.children.length == 0 ? pardom.appendChild(createdom) : pardom.insertBefore(createdom, pardom.children[index])
}
//axios
type axiosmethod = 'GET' | 'POST'
export function fnaxios(method: axiosmethod, url: string, data?: object) {
    switch (method) {
        case 'GET': return axios.get(url, <AxiosRequestConfig<any>>qs.stringify(data))
        case 'POST': return axios.post(url, <AxiosRequestConfig<any>>qs.stringify(data))
        default:
            const n: never = method
            return n;
    }
}
// 图片转base64
export function fnbase(file:File):Promise<any>{
   return new Promise((resolve,)=>{
    const reader = new FileReader();
    reader.addEventListener('load', () => {
        const base64 = reader.result?.toString() || '';
        resolve(base64);
    });
    reader.readAsDataURL(file);
   })
}
//单线程 类型接口
interface singleobj {
    fn: () => void,
    timer?: number,
}
//单线程
export async function single(...arg: singleobj[]) {
    let dsq: number | undefined;
    const pro = (fn: any, timer: number) => {
        return new Promise((resolve: any) => {
            clearTimeout(dsq)
            dsq = setTimeout(() => {
                fn()
                resolve()
            }, timer)
        })
    }
    const list = arg.filter((i) => {
        return typeof i == 'object'
    })
    for (let i = 0; i < list.length; i++) {
        if (!list[i].fn) {
            throw `${list[i]}对象中没有fn属性`
        }
        else if (typeof list[i].fn != 'function') {
            throw `${list[i].fn}不是一个有效的事件`
        }
        else {
            list[i].timer ? await pro(() => { list[i].fn() }, list[i].timer!) :
                await pro(() => { list[i].fn() }, 1000)
        }
    }
}
//返回一个随机数
export function randomNum(max: number, min: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}
//返回一个随机颜色
export function randomColor() {
    return '#' + Math.random().toString(16).slice(2, 8).padEnd(6, '0')
}
//返回随机字符串
export function randomString(len = 6) {
    if (len <= 11) {
        return Math.random().toString(36).slice(2, 2 + len).padEnd(len, '0')
    }
    else {
        const str: string = randomString(11)
        const str2: string = randomString(len - 11)
        return str + str2
    }
}
//数组截取
export function groupArray(att: any[], num: number) {
    const list = [];
    let current: any[] = [];
    att.forEach(res => {
        current.push(res)
        if (current.length === num) {
            list.push(current);
            current = []
        }
    });
    if (current.length) {
        list.push(current)
    }
    return list;
}
//圆周运动 需要在定时器里面执行  或者在回调函数执行
//例子:setInterval(cliceRun(<HTMLDivElement>dom('..clice'),10,10,10),10)
export function cliceRun(dom:HTMLElement,degreeadd:number,ox:number,oy:number) {
    let dataobj = {
      dom: dom,
      degree: 0,
      degreeadd:degreeadd,
      ox: ox,
      oy: oy
    }
    return function () {
      dataobj.degree += dataobj.degreeadd;
      if (dataobj.degree >= 360) {
        dataobj.degree = 0
      }
      let rad = Math.PI / 180 * dataobj.degree;
      let x = Math.sin(rad) * dataobj.ox;
      let y = Math.cos(rad) * dataobj.oy;
      dataobj.dom.style.left = dataobj.dom.offsetLeft + x + 'px';
      dataobj.dom.style.top = dataobj.dom.offsetTop + y + 'px';
    }
}





