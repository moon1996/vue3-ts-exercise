import {createRouter,createWebHistory,RouteRecordRaw} from "vue-router"
import list from './list'
const routes:Array<RouteRecordRaw>=[
    ...list
]
const router=createRouter({
    history:createWebHistory(),
    routes
})
export default router