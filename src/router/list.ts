let list=[
    {
        path:'/',
        name:'index',
        meta:{cname:'首页'},
        component: {render: () => h(resolveComponent("router-view"))},
        redirect: '/index/deme',
        children:[
            {
                path:'/index/deme',
                name:'indexDeme',
                meta:{cname:'首页/deme'},
                component:()=>import('../views/index/deme.vue'),
            },
            {
                path:'/index/deme2',
                name:'indexDeme2',
                meta:{cname:'首页/deme2'},
                component:()=>import('../views/index/deme2.vue'),
            },
            {
                path:'/index/deme3',
                name:'indexDeme3',
                meta:{cname:'首页/deme3'},
                component:()=>import('../views/index/deme3.vue'),
            },
        ]
    },
    
]
export default list