import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import IconsResolver from 'unplugin-icons/resolver'
import Icons from 'unplugin-icons/vite'
const pathSrc = path.resolve(__dirname, 'src')
export default defineConfig({
  plugins: [
    vue({
      reactivityTransform: true, // 启用响应式语法糖$ref $computed $toRef
    }),
    AutoImport({
    imports:['vue','vue-router'],//官方模块
    dirs:['./src/func'], 
    dts:'./src/auto-import.d.ts',
    resolvers: [
      ElementPlusResolver(),
      // Auto import icon components
      // 自动导入图标组件
      IconsResolver({
        prefix: 'Icon',
      }),
    ],
    }),
    Components({
      resolvers: [
        // Auto register icon components
        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep'],
        }),
        // Auto register Element Plus components
        // 自动导入 Element Plus 组件
        ElementPlusResolver(),
      ],

      dts: path.resolve(pathSrc, 'components.d.ts'),
    }),
  

    Icons({
      autoInstall: true,
    }),
  ],
  server:{
    proxy:{
      '/phpapi':{
         target:'http://localhost:80/',
         changeOrigin:true,
         rewrite: (path) => path.replace(/^\/phpapi/, '')
      }
    }
  },
  
})


